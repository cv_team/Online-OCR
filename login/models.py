import uuid

from django.db import models

from login.storage import ImageStorage


class User(models.Model):
    gender = (
        ('male', "男"),
        ('female', "女"),
    )
    name = models.CharField(max_length=128, unique=True)  # 是否唯一
    password = models.CharField(max_length=256)
    email = models.EmailField(unique=True)
    sex = models.CharField(max_length=32, choices=gender, default="男")
    c_time = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.name

    # 元数据
    class Meta:
        ordering = ["-c_time"]  # c_time降序排列
        verbose_name = "用户"
        verbose_name_plural = "用户"  # 复数


class Image(models.Model):
    user_name = models.CharField(max_length=128)  # 是否唯一
    uuid = models.CharField(max_length=128, default='', null=False)
    image = models.ImageField(upload_to='img/', blank=False, storage=ImageStorage())
    content = models.CharField(max_length=128, null=False, default='')

    def __str__(self):
        return 'media/%s' % self.image.name
