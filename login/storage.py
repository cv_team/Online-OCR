# -*- coding: UTF-8 -*-
import uuid

from django.core.files.storage import FileSystemStorage


class ImageStorage(FileSystemStorage):
    from django.conf import settings

    def __init__(self, location=settings.MEDIA_ROOT, base_url=settings.MEDIA_URL):
        super(ImageStorage, self).__init__(location, base_url)

    def _save(self, name, content):
        import os
        uid = str(uuid.uuid1())
        dir_name = os.path.dirname(name)
        ext = os.path.splitext(name)[1]
        name = ''.join(uid.split('-'))
        name = os.path.join(dir_name, name)+ext
        return super(ImageStorage, self)._save(name, content)
