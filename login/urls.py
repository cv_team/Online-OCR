from django.urls import path
from . import views

app_name = 'login'
urlpatterns = [
    path('', views.index, name='index'),
    path('login/', views.login),
    path('index/', views.index),
    path('register/', views.register),
    path('logout/', views.logout),
    path('result/<str:uuid_code>/', views.rusult, name='result'),
    path('result/', views.rusult, name='image_list'),
]
