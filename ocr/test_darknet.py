# -*- coding: utf-8 -*-
import os
import cv2

from ocr.yolo_ocr import predict_model

if __name__ == '__main__':
    image = cv2.imread("../media/img/jisuanji.jpg")
    res=predict_model(image)
    print(res)
