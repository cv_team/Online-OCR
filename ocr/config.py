import os
from onlineocr import settings


########################文字检测################################################
##文字检测引擎 
IMGSIZE = (608, 608)  ## yolo3 输入图像尺寸
yoloTextFlag = 'opencv'  ##keras,opencv,darknet，模型性能 keras>darknet>opencv

############## darknet yolo  ##############

# darknetRoot = os.path.join(os.path.curdir,"darknet")## yolo 安装目录
darknetRoot = "darknet"  ## yolo 安装目录
print(darknetRoot)
yoloCfg = os.path.join(settings.BASE_DIR, "models", "text.cfg")
yoloWeights = os.path.join(settings.BASE_DIR, "models", "text.weights")
yoloData = os.path.join(settings.BASE_DIR, "models", "text.data")
############## darknet yolo  ##############

########################文字检测################################################

## GPU选择及启动GPU序号
GPU = False  ##OCR 是否启用GPU
GPUID = 0  ##调用GPU序号

##vgg文字方向检测模型
DETECTANGLE = True  ##是否进行文字方向检测
AngleModelPb = os.path.join(settings.BASE_DIR, "models", "Angle-model.pb")
AngleModelPbtxt = os.path.join(settings.BASE_DIR, "models", "Angle-model.pbtxt")
AngleModelFlag = 'opencv'  ## opencv or tf

######################OCR模型###################################################
##是否启用LSTM crnn模型
##OCR模型是否调用LSTM层
LSTMFLAG = True
ocrFlag = 'torch'  ##ocr模型 支持 keras  torch opencv版本
##模型选择 True:中英文模型 False:英文模型
chineseModel = True  ## 中文模型或者纯英文模型
OCR_LSTM = os.path.join(settings.BASE_DIR, "models", "ocr-lstm.pth")
OCR_DENSE = os.path.join(settings.BASE_DIR, "models", "ocr-dense.pth")
OCR_ENGLISH = os.path.join(settings.BASE_DIR, "models", "ocr-english.pth")



