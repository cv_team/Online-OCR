# Online-OCR

#### 介绍  
在线OCR

#### 主要界面
登录界面  
![](./doc/登录界面.png)
上传界面  
![](./doc/上传界面.png)
结果界面  
![](./doc/结果界面.png)  
图片列表  
![](./doc/图片列表.png)  

#### 安装教程

安装依赖

1.  Django 2.2.1
2.  Pytorch 1.1
3.  openncv 4.1.0

#### 使用说明

注册界面注册用户  
上传界面上传图像  
![](./doc/目标图像.png)  
得到识别结果  
![](./doc/识别结果.png)  

s

